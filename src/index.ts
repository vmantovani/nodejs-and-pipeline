#! /usr/bin/env node

import { Command } from "commander";
import { Fibonacci } from "./Fibonacci";
const program = new Command();

const fibonacci = new Fibonacci();

program
  .name("string-util")
  .description("CLI to some JavaScript string utilities")
  .version("0.15.0");

program
  .command("n")
  .description("Mostra N-ésimo número da sequência Fibonacci")
  .argument("<string>", "string to split")
  .option("--first", "display just the first substring")
  .option("-s, --separator <char>", "separator character", ",")
  .action((input: any, options: any) => {
    const n_esimo = fibonacci.getNEsimoNumberSequency(input);
    console.log(n_esimo);
  });

program.parse();
