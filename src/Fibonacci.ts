export class Fibonacci {
  constructor() {}

  getNEsimoNumberSequency(number: number) {
    let ultimo = 1;
    let penultimo = 0;
    let n_esimo;

    if (number <= 2) {
      return number - 1;
    }

    for (let index = 2; index < number; index++) {
      n_esimo = ultimo + penultimo;
      penultimo = ultimo;
      ultimo = n_esimo;
    }

    return n_esimo;
  }
}
