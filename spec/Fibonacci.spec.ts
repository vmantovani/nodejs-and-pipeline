import { Fibonacci } from "../src/Fibonacci";

describe("Unity tests for getNEsimoNumberSequency", () => {
  let fibonacci: Fibonacci;

  beforeEach(() => {
    fibonacci = new Fibonacci();
  });

  describe("getNEsimoNumberSequency", () => {
    it("Should return 34 when input value 10", () => {
      const n_esimo = fibonacci.getNEsimoNumberSequency(10);
      expect(n_esimo).toBe(34);
    });

    it("Should return 377 when input value 15", () => {
      const n_esimo = fibonacci.getNEsimoNumberSequency(15);
      expect(n_esimo).toBe(377);
    });

    it("Should return 0 when input value 1", () => {
      const n_esimo = fibonacci.getNEsimoNumberSequency(1);
      expect(n_esimo).toBe(0);
    });

    it("Should return 1 when input value 2", () => {
      const n_esimo = fibonacci.getNEsimoNumberSequency(2);
      expect(n_esimo).toBe(1);
    });
  });
});
